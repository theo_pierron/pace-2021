#include <vector>
#include <assert.h>
#include "time.h"
#include <csignal>
#include <functional>
#include <cstring>

#include "../shared/graph.hpp"
#include "../shared/solution.hpp"
#include "instance.hpp"
#include "lp_solve.hpp"
#include "../heuristic/src/multi_cc_instance.h"
#include "../heuristic/src/union_find.h"
#include "../heuristic/src/local_search.h"
#include "../heuristic/src/simulated_annealing.h"

using namespace std;

void dfs_aux(Graph *g, vector<int> &marks, Node v)
{
    for (Node w : g->neighbours(v))
    {
        if (marks[w] == -1)
        {
            marks[w] = marks[v];
            dfs_aux(g, marks, w);
        }
    }
}

vector<Graph> dfs(Graph *g)
{
    int n = g->nr_vertices();
    vector<int> marks(n, -1);
    int i = 0;
    for (int u = 0; u < n; u++)
    {
        if (marks[u] == -1)
        {
            marks[u] = i;
            dfs_aux(g, marks, u);
            i++;
        }
    }
    vector<Graph> cc;
    for (int j = 0; j < i; j++)
    {
        vector<Node> nodes;
        vector<Edge> edges;
        for (int u = 0; u < n; u++)
        {
            if (marks[u] == j)
            {
                nodes.push_back(u + 1);
                for (Node v : g->neighbours(u))
                {
                    assert(marks[v] == j);
                    if (u < v)
                        edges.push_back(make_pair(u + 1, v + 1));
                }
            }
        }
        cc.push_back(Graph(nodes, edges));
    }
    return cc;
}

Solution run_algo(Graph* g, function<bool(int)> stop)
{
    // Create instance from graph
    ExactInstance instance(*g, g->nb_edges(), 0);

    // Preprocess
    instance.preprocess();

    // Create instance for heuristic
    KernelizedMultiCCInstance heuristic(g->nr_vertices(), g->all_edges());
    auto timeout = [](int i) { return i < 5000; };
    double initial_t = 20;
    double decay_r = 0.99975;
    auto temp = [&](double cur_T, int64_t it, int64_t n) -> double {
        it = it % 150;
        double res = initial_t / (1 + it + log(n));
        return res;
    };

    // Compute heuristic
    heuristic.sa_multi_cc(timeout, temp, 40, 50, initial_t, 0);
    // Get Solution from heuristic
    int ub_heuristic = heuristic.count_sol();
    vector<Cluster> cluster_heuristic = heuristic.get_sol();
    Solution heur = Solution(ub_heuristic, g->nr_vertices(), cluster_heuristic);

    // Set upper bound for the instance
    instance.set_upper_bound(heur.get_cost());

    // Initialize lower bound
    clock_t start = clock();
    instance.init_lower_bound();
    clock_t end = clock();

    // Recompute lower bound if enough time left
    double time_for_lb = double(end - start) / CLOCKS_PER_SEC;
    int nr_recompute_lb = 0;
    if (time_for_lb <= 3)
        nr_recompute_lb = 19;
    else if (time_for_lb > 3 && time_for_lb <= 5)
        nr_recompute_lb = 11;
    else if (time_for_lb > 5 && time_for_lb <= 20)
        nr_recompute_lb = 2;

    if (instance.get_upper_bound() == instance.lower_bound())
      nr_recompute_lb = 0;

    for (int i = 0; i < nr_recompute_lb; i++)
    {
      if (!stop(0)) throw 0;
      instance.shuffle_graph();
      instance.recompute_lower_bound();
    }

    //cout << "LB: " << instance.lower_bound() << " UB: " << instance.get_upper_bound() << endl;

    // Apply forced moves
    instance.forced_moves();

    // Compute solution
    Solution res = instance.algo_brute(stop);

    // Get optimal solution
    if (res.get_cost() < heur.get_cost())
    {
        heur = res;
    }
    return heur;
}

vector<Edge> get_cost(Graph* G,function<bool(int)>stop ){

  int cost = 0;
  vector<Graph> cc = dfs(G);
  vector<int> cluster;

  vector<Edge> result(0);
  for (Graph g : cc) {
    Graph save(g);
    g.shuffle();
    vector<Edge> add(0);

    if (save.nr_vertices() > 1 && save.nr_vertices() < 90) // < 120
      {
	Solution lp_sol = lp_solve(&g);
	if (lp_sol.get_cost() != std::numeric_limits<int>::max())
	  {
	    add = lp_sol.get_edges(&save);
	    result.insert(result.end(), add.begin(), add.end());
	    continue;
	  }
      }
    /*Else go normal*/
    Solution sol = run_algo(&g,stop);
    add = sol.get_edges(&save);
    result.insert(result.end(), add.begin(), add.end());
  }
  return result;
}

volatile sig_atomic_t tle = 0;

void term(int signum)
{
  (void) signum;
  tle = 1;
}

LowMemGraph kernelize(Graph* g)
{
  // Create instance from graph
  ExactInstance instance(*g, g->nb_edges(), 0);
  
  // Preprocess
  instance.preprocess();
  // Create instance for heuristic
  vector<Edge> edges = g->all_edges();
  vector<Edge> initial_edges;
  initial_edges.reserve(edges.size());
  for (auto &[u,v]: edges)
    initial_edges.emplace_back(min(u, v), max(u, v));
  sort(initial_edges.begin(), initial_edges.end());
  LowMemGraph model(g->nr_vertices(), initial_edges);
  model.kernelize();
  return model;
}

vector<Edge> sym_diff (vector<Edge> modified, vector<Edge> heur) {
  vector<Edge> result(0);
  int i=0;
  int j=0;
  while (i < modified.size() || j < heur.size()) {
    if (i >= modified.size()) {
      result.push_back(heur[j]);
      j++;
      continue;
    }
    if (j >= heur.size()) {
      result.push_back(modified[i]);
      i++;
      continue;
    }
    if (modified[i] == heur[j]) {
      i++;
      j++;
      continue;
    }
    if (modified[i] < heur[j]) {
      result.push_back(modified[i]);
      i++;
    } else {
      result.push_back(heur[j]);
      j++;
    }
  }
  return result;
}

void kernelize_wrap() 
{
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_handler = term;
  sigaction(SIGTERM, &action, NULL);
  cin.tie(0);
  ios::sync_with_stdio(false);
  Graph *G = Graph::load_cin(cin);
  vector<Edge> modified(0);


  Graph save(*G);
  Graph save2(*G);
  LowMemGraph ker = kernelize(G);
  auto ccs = ker.connected_components();

  vector<int> vertex_to_cc(ker.n());
  int n_cc = ccs.size();
  for (int i = 0; i < n_cc; ++i)
    for (int v : ccs[i])
      vertex_to_cc[v] = i;
  
  vector<int64_t> ccs_m(n_cc, 0);
  for (int u = 0; u < ker.n(); u++)
    for (int v: ker.neighbors(u)) 
      if (u<v) ccs_m[vertex_to_cc[u]]++;

  vector<bool> to_remove(ker.n(),false);
  int nb_remove = 0;
  int edges_remove = 0;
  for (int i = 0; i < n_cc; ++i)
    {
      int64_t m = ccs_m[i], n = (int64_t)ccs[i].size();
      if ((n <= 3) || (m == (n * (n-1) / 2))) {
	nb_remove+=ccs[i].size();
	edges_remove+=ccs_m[i];
	for (int u:ccs[i])
	  to_remove[u]=true;
      }
    }
  vector<int> newlabel(ker.n(),-1);
  int label = 0;
  for (int u = 0; u < ker.n(); u++) {
    if (!to_remove[u]) {
      newlabel[u]=label+1;
      label++;
    }
    //cout << "new for " << u << " is " << newlabel[u] << endl;
  }
    

  for (Node u = 0; u < save2.nr_vertices(); u++){
    for (Node v = u+1; v < save2.nr_vertices(); v++) {
      if (ker.adjacent(u,v) != save2.has_edge(u,v)) {
	modified.push_back(Edge(u,v));
	//cout << save2.labels[u] << " " << save2.labels[v] << endl;
      }
    }
  }
  try {
    auto stop = [&](int i){ (void) i; return tle == 0; };
    vector<Edge> result = get_cost(&save,stop);
    cout << result.size() << endl;
    cout << "p cep 0 0" << endl;
    /* 4 heuristics
       Uncomment this block before submitting to optil.io
       
    for (int i = 1; i<=4; i++) {
      cout << result.size() << endl;
      for (Edge e:result) {
	cout << e.first << " " << e.second << endl;
      }
    }
    */
    cout.flush();
  }
  catch(int code) 
  {
    cout << modified.size() << endl;
    cout << "p cep " << ker.n()-nb_remove << " " << ker.m()-edges_remove << endl;
    for (Node u=0; u < ker.n(); u++) {
      if (to_remove[u]) continue;
      for (Node v: ker.neighbors(u)) {
	if (to_remove[v]) continue;
	if (v > u) cout << newlabel[u] << " " << newlabel[v] << endl;
      }
    }
    /* 4 heuristics
       Uncomment this block before submitting to optil.io

    vector<Edge> heur = sym_diff(modified,ker.heuristic1(to_remove)); // vraiment une diff sym ? 
    cout << heur.size() << endl;
    for (Edge e: heur) {
      cout << save2.labels[e.first] << " " << save2.labels[e.second] << endl;
    }
    
    heur = sym_diff(modified,ker.heuristic2(to_remove));
    cout << heur.size() << endl;
    for (Edge e: heur) {
      cout << save2.labels[e.first] << " " << save2.labels[e.second] << endl;
    }
    
    heur = sym_diff(modified,ker.heuristic3(newlabel));
    cout << heur.size() << endl;
    for (Edge e: heur) {
      cout << save2.labels[e.first] << " " << save2.labels[e.second] << endl;
    }
    
    heur = sym_diff(modified,ker.heuristic4(newlabel));
    cout << heur.size() << endl;
    for (Edge e: heur) {
      cout << save2.labels[e.first] << " " << save2.labels[e.second] << endl;
    }
    */
  }
}


void lift() {
  Graph *G = Graph::load_cin(cin);
  std::string line;
  cin >> line;
  assert(line == "#");
  int cost;
  cin >> cost;
  Graph *H = Graph::load_cin(cin);
  cin >> line;
  assert(line == "#");

  if (H->nr_vertices() == 0) { // solve directly
    vector<Edge> result = get_cost(G,[&](int i) {(void) i; return true;});
    for (Edge e: result){
      cout << e.first << " " << e.second << endl;
    }
    return;
  }

  vector<Edge> solution(0);
  int u,v;
  while (cin >> u >> v) {
    solution.push_back(Edge(u,v));
  }

  if (cost == 0) { // no modification done
    for (Edge e:solution) {
      cout << e.first << " " << e.second << endl;
    }
    return;
  }

  // kernelize again to get right labeling

  Graph save2(*G);
  LowMemGraph ker = kernelize(G);
  auto ccs = ker.connected_components();

  vector<int> vertex_to_cc(ker.n());
  int n_cc = ccs.size();
  for (int i = 0; i < n_cc; ++i)
    for (int v : ccs[i])
      vertex_to_cc[v] = i;
  
  vector<int64_t> ccs_m(n_cc, 0);
  for (int u = 0; u < ker.n(); u++)
    for (int v: ker.neighbors(u)) 
      if (u<v) ccs_m[vertex_to_cc[u]]++;
    
  vector<bool> to_remove(ker.n(),false);
  int nb_remove = 0;
  int edges_remove = 0;
  for (int i = 0; i < n_cc; ++i)
    {
      int64_t m = ccs_m[i], n = (int64_t)ccs[i].size();
      if ((n <= 3) || (m == (n * (n-1) / 2))) {
	nb_remove+=ccs[i].size();
	edges_remove+=ccs_m[i];
	for (int u:ccs[i]) to_remove[u]=true;
      }
    }
  vector<int> oldlabel(ker.n()-nb_remove+1,0);
  int label = 0;
  for (int u = 0; u < ker.n(); u++) {
    if (!to_remove[u]) {
      oldlabel[label+1]=u;
      label++;
    }
  }

  // recover solution
  vector<Edge> modified(0);
  for (Node u = 0; u < save2.nr_vertices(); u++){
    for (Node v = u+1; v < save2.nr_vertices(); v++) {
      if (ker.adjacent(u,v) != save2.has_edge(u,v)) {
	modified.push_back(Edge(u,v));
      }
    }
  }
  for (int i = 0; i < solution.size(); i++) {
    solution[i]=Edge(oldlabel[solution[i].first], oldlabel[solution[i].second]);
  }
  sort(solution.begin(),solution.end());
  vector<Edge> heur = sym_diff(modified,solution); 
  for (Edge e: heur) {
    cout << save2.labels[e.first] << " " << save2.labels[e.second] << endl;
  }
}




int main(int argc, char *argv[]){
  kernelize_wrap(); // Comment for lifting solutions
  //lift(); // Uncomment for lifting solutions
  return 0;
}
    
