# PACE 2021

Submissions for the kernelization track of PACE 2021. 
Login on optil.io: tpierron, pgase, tp2, GBathie

## Installation

Run the following commands in the directory where CMakeLists.txt is located:

```bash
cmake .
make
```

## Usage for kernelization

```bash
mainoptil < <path to .gr file>
```

## Usage for lifting

* Comment line 430 in exact/main_optil.cpp
* Uncomment line 431 in exact/main_optil.cpp
* Recompile, then run with: 

```bash
mainoptil < <path to input file>
```

